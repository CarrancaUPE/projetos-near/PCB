# PCB
A máquina PCB maker automatiza o processo de fabricação
da placa de circuito impresso. Nas diversas etapas de fabricação
(centrifugação, secagem, luz UV e corrosão) pode-se ecolher entre 
os modos automático e manual.

## Automático
A partir de uma sequência automática das etapas mencionadas acima.

## Manual
Haverá uma interação com o usuário, o qual definirá os intervalos 
de tempo de cada etapa e a ordem delas.

## Etapas

*Centrifugação
*Secagem
*Luz UV
*Corrosão



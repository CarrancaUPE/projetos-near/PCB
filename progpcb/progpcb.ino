#include <Keypad.h>
#include <EEPROM.h>
#include <LiquidCrystal.h>

const byte ROWS = 4; // Four rows
const byte COLS = 4; // Three columns

int endereco; ///////

char keys[ROWS][COLS] = {
  {'1', '2', '3', 'A'},
  {'4', '5', '6', 'B'},
  {'7', '8', '9', 'C'},
  {'*', '0', '#', 'D'}
};
// Connect keypad ROW0, ROW1, ROW2 and ROW3 to these Arduino pins.
byte rowPins[ROWS] = {6, 7, 8, 9};
// Connect keypad COL0, COL1 and COL2 to these Arduino pins.
byte colPins[COLS] = {A3, A2, A1, A0};

// Create the Keypad
Keypad kpd = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

int contador = 0;
int buzzer = 13;
int releuv = 0;
int releSecador = 1;
int timeCent = 11;
int timeSecagem = 11;
int timeLV = 16;
int timeCorrosao = 11;
int tempo_Centrifuga = 11000;
int tempo_Secagem = 11000;
int tempo_LV = 16000;
int tempo_Corrosao = 11000;
int MotorPin = 10;

char key;

void modoautomatico();
void displaylcd();
void modomanual();
void configuracoes();
void ajuda();
void centrifuga();
void secagem();
void luzvioleta();
void corrosao();

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

void setup()
{
  pinMode(MotorPin, OUTPUT);
  pinMode(releuv, OUTPUT);
  pinMode(releSecador, OUTPUT);
  pinMode(A0, INPUT);
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);
  pinMode(A3, INPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);

  lcd.begin(20, 4);
  digitalWrite(releSecador, HIGH);
  digitalWrite(releuv, HIGH);
  digitalWrite(MotorPin, LOW);

  timeCent = EEPROM.read(1);
  timeSecagem = EEPROM.read(3);
  timeLV = EEPROM.read(5);
  timeCorrosao = EEPROM.read(7);
  tempo_Centrifuga = EEPROM.read(0);
  tempo_Secagem = EEPROM.read(2);
  tempo_LV = EEPROM.read(4);
  tempo_Corrosao = EEPROM.read(5);

}

void loop()
{
  displaylcd();
}

void displaylcd()
{
  unsigned long tempo;
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Ola! Seja bem vindo");
  lcd.setCursor(0, 1);
  lcd.print("a produtora PCBMaker");
  lcd.setCursor(0, 2);
  lcd.print("2000, by Carranca22");
  delay(5500);
  lcd.clear();
  lcd.setCursor(0, 1);
  lcd.print("Por favor, digite o numero:");
  delay(1500);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("1 - Modo Automatico");
  lcd.setCursor(0, 1);
  lcd.print("2 - Modo Manual");
  lcd.setCursor(0, 2);
  lcd.print("3 - Configuracoes");
  lcd.setCursor(0, 3);
  lcd.print("4 - Ajuda");
  do {
    key = kpd.getKey();
    if (key == '1')
      modoautomatico();
    else if (key == '2')
      modomanual();
    else if (key == '3')
      configuracoes();
    else if (key == '4')
      ajuda();
  } while (key != '#');
}

void confirme(void) {
  lcd.print("Pressione *");

  do {
    key = kpd.getKey();
  } while (key != '*');
}

void modoautomatico()
{
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Incializando...");
  delay(4000);

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Lave a placa");
  lcd.setCursor(0, 1);
  lcd.print("->Seque a placa");
  lcd.setCursor(0, 2);
  lcd.print("->Pinte a placa");
  lcd.setCursor(0, 3);

  confirme();

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Centrifuga");

  delay(5000);
  centrifuga();

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("->Vamos usar");
  lcd.setCursor(0, 1);
  lcd.print("a secagem");
  delay(5000);
  secagem();
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("->Colocar o");
  lcd.setCursor(0 , 1);

  lcd.print("fotolito");

  delay(5000);
  lcd.clear();
  lcd.cursor();
  lcd.setCursor(0, 0);
  lcd.print("-> Apos o termino");
  lcd.setCursor(0, 1);
  lcd.print("Pressione o botao *");
  do {
    key = kpd.getKey();
  } while (key != '*');
  lcd.clear();
  lcd.setCursor(0, 0);

  lcd.print("->Usar a luz ultravioleta");

  delay(5000);
  luzvioleta();
  lcd.clear();
  lcd.setCursor(0, 0);


  lcd.print("->Usar a barrilha");

  delay(5000);
  lcd.clear();
  lcd.cursor();
  lcd.setCursor(0, 0);
  lcd.print("-> Apos o termino");
  lcd.setCursor(0, 1);
  lcd.print("Pressione o botao *");
  do {
    key = kpd.getKey();
  } while (key != '*');
  lcd.clear();
  lcd.cursor();
  lcd.setCursor(0, 0);

  lcd.print("->Retirar a tinta");

  delay(5000);
  lcd.clear();
  lcd.cursor();
  lcd.setCursor(0, 0);
  lcd.print("-> Apos o termino");
  lcd.setCursor(0, 1);
  lcd.print("Pressione o botao *");
  do {
    key = kpd.getKey();
  } while (key != '*');
  lcd.clear();
  lcd.cursor();
  lcd.setCursor(0, 0);
  lcd.print("->Novamente usar");
  lcd.setCursor(0, 1);

  lcd.print("a luz UV");

  delay(5000);
  luzvioleta();
  lcd.clear();
  lcd.cursor();
  lcd.setCursor(0, 0);

  lcd.print("->Usar o hipercloreto");
  delay(5000);
  corrosao();
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("->Lavar placa");
  delay(5000);
  lcd.setCursor(0, 1);
  lcd.print("->Usar soda custica");
  delay(5000);
  lcd.setCursor(0, 2);
  lcd.print("->Cortar e lixar placa");
  delay(5000);
  lcd.setCursor(0, 3);

  lcd.print("->Trabalhar as legendas");
  delay(5000);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("->Agora, repetir as etapas de:");
  delay(5000);
  lcd.setCursor(0, 1);
  lcd.print("->Limpar, Pintar, Centrifugar, Secar");
  delay(5000);
  lcd.setCursor(0, 2);
  lcd.print("->Colocar a mascara");
  delay(5000);
  lcd.setCursor(0, 3);
  lcd.print("->Utilizar barrilha");
  delay(5000);
  lcd.clear();
  lcd.setCursor(5, 1);
  lcd.print("Processo Finalizado");
  displaylcd();
}

void modomanual() {
  unsigned long tempo;
  lcd.clear();
  lcd.cursor();
  lcd.blink();
  lcd.setCursor(0, 0);
  lcd.print("Qual processo");
  lcd.setCursor(0, 1);
  lcd.print("deseja ativar?");
  lcd.setCursor(0, 2);
  lcd.print("Aperte D para sair");
  delay(3000);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("1 - Centrifuga");
  lcd.setCursor(0, 1);
  lcd.print("2 - Secagem");
  lcd.setCursor(0, 2);
  lcd.print("3 - Luz UV");
  lcd.setCursor(0, 3);
  lcd.print("4 - Corrosao");
  do {
    key = kpd.getKey();
    if (key == '1')
      centrifuga();
    else if (key == '2')
      secagem();
    else if (key == '3')
      luzvioleta();
    else if (key == '4')
      corrosao();
    else if (key == 'D')
      displaylcd();
  } while (key != '#');
}

void configuracoes()
{
  unsigned long tempo;
  lcd.clear();

  lcd.setCursor(0, 0);
  lcd.print("Mudar tempo de uma");
  lcd.setCursor(0, 1);
  lcd.print("funcao");
  lcd.setCursor(0, 3);
  lcd.print("Escolha a funcao...");
  delay(7000);

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Digite o numero da funcao");
  delay(8000);

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("1 - Centrifuga");
  lcd.setCursor(0, 1);
  lcd.print("2 - Secagem");
  lcd.setCursor(0, 2);
  lcd.print("3 - Luz Violeta");
  lcd.setCursor(0, 3);
  lcd.print("4 - Corrosao");

  tempo = millis();

  do {
    key = kpd.getKey();
    if (key == '1') {
      timeCent = coletarTempo();
      tempo_Centrifuga *= 1000; /* */

      endereco = 0;
      EEPROM.write(endereco, tempo_Centrifuga);
      endereco = 1;
      EEPROM.write(endereco, timeCent);

      lcd.setCursor(0, 0);
      lcd.print(tempo_Centrifuga);
    }
    else if ( key == '2') {
      tempo_Secagem = coletarTempo();
      timeSecagem = tempo_Secagem / 1000;

      endereco = 2;
      EEPROM.write(endereco, tempo_Secagem);
      endereco = 3;
      EEPROM.write(endereco, timeSecagem);

      lcd.print(tempo_Secagem);
    }
    else if ( key == '3') {
      tempo_LV = coletarTempo();
      timeLV = tempo_LV / 1000;
      endereco = 4;
      EEPROM.write(endereco, tempo_LV);
      endereco = 5;
      EEPROM.write(endereco, timeLV);
      lcd.print(tempo_LV);
    }
    else if ( key == '4') {
      tempo_Corrosao = coletarTempo();
      timeCorrosao = tempo_Corrosao / 1000;
      endereco = 6;
      EEPROM.write(endereco, tempo_Corrosao);
      endereco = 7;
      EEPROM.write(endereco, timeCorrosao);
      lcd.print(tempo_Corrosao);
    }
  } while ((millis() - tempo) < 60000);
  lcd.clear();
  displaylcd();
}

int coletarTempo() {
  String str = ""
               char key;

  while (true) {
    key = kpd.getKey();

    if (key && key != '*') {
      str += String(key);
    }
    else if (key != '*')
      break
    }
  return str.toInt();
}

void ajuda()
{
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Bem vindo a Ajuda");
  delay(2000);

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Qual funcao qr ajuda?");
  lcd.setCursor(0, 1);
  lcd.print("1- Modo Automatico");
  lcd.setCursor(0, 2);
  lcd.print("2- Modo Manual");
  lcd.setCursor(0, 3);
  lcd.print("3 - Configuracoes");

  do {
    key = kpd.getKey();
    if (key == '1') {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("No modo automatico");
      lcd.setCursor(0, 1);
      lcd.print("ira aparecer todo o");
      lcd.setCursor(0, 2);
      lcd.print("passo a pass p / fazer");
      lcd.setCursor(0, 3);
      lcd.print("a placa PCB");
      delay(4000);
    }
    else if (key == '2') {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("No modo manual ira");
      lcd.setCursor(0, 1);
      lcd.print("aparecer apenas as");
      lcd.setCursor(0, 2);
      lcd.print("funcoes de processo");
      lcd.setCursor(0, 3);
      lcd.print("p / vc escolher uma");
      delay(4000);
    }
    else if (key == '3') {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Nas configuracoes vc");
      lcd.setCursor(0, 1);
      lcd.print("vai pode alterar o");
      lcd.setCursor(0, 2);
      lcd.print("tempo q cada funcao");
      lcd.setCursor(0, 3);
      lcd.print("vai ter de processp");
      delay(4000);
    }
  } while (key != '#');
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Feito pelo Carranca");
  delay(3000);
  dislpaylcd():
  }
}

void centrifuga()
{   
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Centrifugando...");
  
  operacao(0, MotorPin);
}

void secagem()
{
  //15 minutos quente
  //2 minutos frio (opcional)
  
  confirme();
  
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Secando...");
  delay(3000);
  
  lcd.setCursor(0, 1);
  operacao(1, releSecador);
}

void operacao(int endereco, int pin){
  
  long int tempo = millis();
  int tempoProcesso = EEPROM.read(endereco) * 1000; // tranformando segundos em milisegundos
  int contador = 1;
  
  digitalWrite(pin, HIGH);

  lcd.print(0);
  while(millis() - tempo < tempoProcesso)
  {
    if(millis() - tempo < 1000 * contador)
      lcd.print(contador);
      contador++;
  }
  
  digitalWrite(pin, LOW);

  alerta()
}

void luzvioleta()
{
  //3 minutos
  
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Luz UV");
  
  operacao(2, releuv) 
}

void corrosao()
{
  //15 a 20 minutos

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Corrosao...");
  delay(3000);
  
  confirme();
  
  lcd.print("Corroendo");
  operacao(3, releSecador);
}

void alerta()
{
  lcd.cleaar();
  lcd.setCursor(0, 0);
  lcd.print("Aperte * para desligar");
  
  do {
    tone(buzzer, 261);
    key = kpd.getKey();
  } while (key != '*');
  noTone(buzzer);
}
